package com.tezzt.sharedservice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class SolutionForJudgeDTO {
    private Long id;
    private String path;
    private String exercicePath;
}

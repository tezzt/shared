package com.tezzt.sharedservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class SharedServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SharedServiceApplication.class, args);
	}

}
